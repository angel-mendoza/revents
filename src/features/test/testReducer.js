import {createReducer} from '../../app/common/util/reducerUtils'
import {INCREMENT_COUNTER, DECREMENT_COUNTER} from './testConstants'

const initialState = {
  data: 42
}

const increment = (state) => {
  return {...state, data: state.data + 1}
}

const decrement = (state) => {
  return {...state, data: state.data - 1}
}

export default createReducer(initialState, {
  [INCREMENT_COUNTER]: increment,
  [DECREMENT_COUNTER]: decrement,
})