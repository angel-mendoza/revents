import React, {Component} from 'react'
import {connect} from 'react-redux'

class TestComponent extends Component{
  render(){
    return(
      <div>
        <h1>test Component</h1>
        <h3>data: {this.props.data}</h3>
      </div>
    )
  }
}

const mapState = (state) => ({
  data: state.data
})

export default connect(mapState)(TestComponent) 
