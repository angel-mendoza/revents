import { CREATE_EVENTS, UPDATE_EVENTS, DELETE_EVENTS } from "./eventConstants"

export const createEvent = (event) => {
  return {
    type: CREATE_EVENTS,
    payload: {
      event
    }
  }
}

export const updateEvent = (event) => {
  return {
    type: UPDATE_EVENTS,
    payload: {
      event
    }
  }
}

export const deleteEvent = (eventId) => {
  return {
    type: DELETE_EVENTS,
    payload: {
      eventId
    }
  }
}